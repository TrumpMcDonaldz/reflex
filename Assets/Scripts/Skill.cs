﻿using System;
using System.Threading.Tasks;

public class Skill
{
    public Skill(float BCD, Action<Player, Skill> TS, float DM)
    {
        BaseCoolDown = CoolDown = BCD;

        TheSkill = TS;

        IsProcing = false;

        DamageMultiplier = DM;
    }

    public float DamageMultiplier;

    public float BaseCoolDown;

    public float CoolDown; //One Time Use

    public DateTime StartTime;

    public Action<Player, Skill> TheSkill; //Procing Player followed by Procing Skill

    private bool IsProcing;

    public async Task Proc(Player ThePlayer, float CoolDownOverride = -1)
    {
        IsProcing = true;

        if (CoolDownOverride == -1)
        {
            CoolDownOverride = CoolDown;
        }

        TheSkill.Invoke(ThePlayer, this);

        await Task.Delay(TimeSpan.FromSeconds(CoolDownOverride / ThePlayer.Stat.CurrentTimeScale)); //Higher TimeScale > Lesser CoolDown

        CoolDown = BaseCoolDown;

        IsProcing = false;
    }                   
}