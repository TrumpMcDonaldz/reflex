﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour 
{
	//Variables

	public Rigidbody RB;

	public Stats Stat;

	public bool Walker, Seeker;

	public List<Skill> Skills = new List<Skill>(); //Procing Player followed by Skill CoolDown in Seconds
	
	void FixedUpdate()
	{
		Movement();

		Gravity();
	}
	void Update()
	{
		if (Input.GetMouseButton(0))
		{
			Walker = true;
		}

		else
		{
			Walker = false;
		}

		if (Input.GetMouseButton(2))
		{
			Seeker = true;
		}

		else
		{
			Seeker = false;
		}

		Rotation();
	}

	void Movement()
	{
		var Dir = Vector3.zero;

		if (Walker)
		{
			print("Ye");

			var Ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(Ray, out RaycastHit Hit, Mathf.Infinity))
			{
				Dir = (Hit.point - this.transform.position).normalized;

				Dir.y = this.transform.position.y;

				if (Seeker)
				{
					var LookPos = Hit.point;

					LookPos.y = this.transform.position.y;

					this.transform.LookAt(LookPos);
				}
			}
		}

		RB.velocity = new Vector3(Dir.x * Stat.Speed, RB.velocity.y, Dir.z * Stat.Speed);
	}

	void Gravity()
	{
		RB.AddForce(Physics.gravity * Stat.CurrentTimeScale, ForceMode.Acceleration);
	}

	void Rotation()
	{
		var Dir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

		if (Dir == Vector3.zero)
		{
			return;
		}

		this.transform.LookAt(this.transform.position + Dir);
	}

	private void OnTriggerEnter(Collider other)
	{
		print("InYeyeyeye");

		if (Stat.ReflexOn)
		{
			var OtherStat = other.GetComponent<Stats>();

			if (OtherStat != default)
			{
				OtherStat.CurrentTimeScale = Stat.ReflexTimeScale;
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (Stat.ReflexOn)
		{
			var OtherStat = other.GetComponent<Stats>();

			if (OtherStat != default)
			{
				OtherStat.CurrentTimeScale = 1;
			}
		}
	}
}
