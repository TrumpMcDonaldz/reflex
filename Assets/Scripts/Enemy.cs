﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Rigidbody RB;

    public Stats Stat;

    private GameObject Player;

    public bool IsAttacking = false;

    public GameObject ProjPrefab;

    private void Start()
    {
        Player = GameObject.Find("PlaceHolderPlayer");  
    }

    private void FixedUpdate()
    {
        Movement();

        Gravity();
    }

    void Movement()
    {
        if (!IsAttacking)
        {
            Attack();
        }

        var Dir = (Player.transform.position - this.transform.position).normalized;

        Dir.y = RB.velocity.y;

        RB.velocity = Dir * Stat.Speed;
    }

    void Gravity()
    {
        RB.AddForce(Physics.gravity * Stat.CurrentTimeScale, ForceMode.Acceleration);
    }

    async Task Attack()
    {
        IsAttacking = true;

        var Proj = Instantiate(ProjPrefab);

        Proj.transform.position = this.transform.position;

        Proj.layer = LayerMask.NameToLayer("Enemy");

        var ProjStats = Proj.GetComponent<Stats>();

        ProjStats.BaseDamage = Stat.Damage;

        ProjStats.BaseSpeed = 50;

        ProjStats.BaseMass = 1;

        ProjStats.Health.x = 1;

        Proj.GetComponent<Projectile>().Target = Player.transform.position;

        //Proj.GetComponent<Collider>().enabled = true;

        await Task.Delay(TimeSpan.FromSeconds(Stat.CoolDown));

        IsAttacking = false;
    }
}
