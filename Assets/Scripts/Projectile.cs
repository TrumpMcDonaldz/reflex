﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //Variables

    public Player Player;

    public Stats Stat;

    public Vector3 Target;

    public Rigidbody RB;

    private Vector3 Dir;

    public void Start()
    {
        Dir = Target - this.transform.position;

        Dir.Normalize();

        Destroy(this.gameObject, 10);
    }

    private void FixedUpdate()
    {
        RB.velocity = Dir * Stat.Speed;
    }

    private void OnCollisionEnter(Collision collision) //Deal Damage
    {
        var OtherStat = collision.collider.GetComponent<Stats>();

        if (OtherStat == default)
        {
            return;
        }

        //On Hit Callback

        OtherStat.TakeDamage(Stat.BaseDamage);
    }
}
