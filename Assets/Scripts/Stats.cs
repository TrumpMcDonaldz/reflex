﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Stats : MonoBehaviour
{
    [Header("Stats")]
    public Vector2 Health;

    public float BaseSpeed;

    public float BaseMass;

    public float BaseDamage;

    public float BaseCoolDown;

    public float ReflexSize;

    public float ReflexTimeScale; //Time scale set to opposing entities whenever they enter the reflex zone. A value of 1 leave stats unchanged.

    public bool ReflexOn;

    //Non-Base Stats

    public float Damage;

    public float Speed;

    public float CoolDown;

    public List<Collider> InReflexZone;

    public float CurrentTimeScale
    {
        //Time Scale Machine

        get
        {
            return _CurrentTimeScale;
        }

        set
        {
            //Grab Comps

            var RB = this.GetComponent<Rigidbody>();

            var Particle = this.transform.GetComponent<ParticleSystem>();

            var Anim = this.transform.GetComponent<Animator>();

            _CurrentTimeScale = value;

            //Affect Movement Speed

            Speed = BaseSpeed * _CurrentTimeScale;

            //Affect Physics

            if (RB != default)
            {
                //Make sure that Resultant Force is the same

                RB.mass = BaseMass * (1 / _CurrentTimeScale);
            }

            //Affect Particle System

            if (Particle != default)
            {
                var PM = Particle.main;

                PM.simulationSpeed = _CurrentTimeScale;
            }

            //Affect Animator

            if (Anim != default)
            {
                Anim.speed = _CurrentTimeScale;
            }
        }
    }


    private float _CurrentTimeScale; //The entities' present timescale. Default is 1.


    public void Start()
    {
        Damage = BaseDamage;

        CoolDown = BaseCoolDown;

        CurrentTimeScale = 1;
    }

    private void Update()
    {
        if (ReflexOn)
        {
            if (!Input.GetKey(KeyCode.LeftShift))
            {
                InReflexZone.Clear();

                var _InReflexZone = Physics.OverlapSphere(this.transform.position, ReflexSize / 2).Where(x => x.GetComponent<Stats>() != default).ToList();

                if (_InReflexZone == default)
                {
                    foreach (var x in InReflexZone)
                    {
                        x.GetComponent<Stats>().CurrentTimeScale = 1;
                    }

                    InReflexZone.Clear();

                    return;
                }

                var OldInReflexZone = new List<Collider>();

                if (InReflexZone == default)
                {
                    InReflexZone = new List<Collider>();
                }

                OldInReflexZone.AddRange(InReflexZone);

                InReflexZone.RemoveAll(x => !_InReflexZone.Contains(x));

                foreach (var x in _InReflexZone)
                {

                    if (InReflexZone.Contains(x))
                    {
                        continue;
                    }

                    InReflexZone.Add(x);
                }

                foreach (var x in InReflexZone)
                {
                    var xStats = x.GetComponent<Stats>();

                    xStats.CurrentTimeScale = ReflexTimeScale;
                }

                foreach (var OldReflex in OldInReflexZone)
                {
                    if (InReflexZone.Contains(OldReflex))
                    {
                        continue;
                    }

                    OldReflex.GetComponent<Stats>().CurrentTimeScale = 1;
                }
            }

            else
            {
                print("Shifting ");

                foreach (var x in InReflexZone)
                {
                    if (x == default)
                    {
                        continue;
                    }

                    x.GetComponent<Stats>().CurrentTimeScale = 1;
                }
            }
        }
    }

    public void TakeDamage(float Damage)
    {
        Health.x -= Damage;

        if (Health.x <= 0)
        {
            Health.x = 0;

            Destroy(this.gameObject);
        }   
    }
}
