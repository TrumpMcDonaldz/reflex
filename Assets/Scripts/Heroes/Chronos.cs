﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Chronos : Player
{
    private void Awake()
    {
        Stat.BaseDamage = 20;

        Stat.BaseCoolDown = 2;

        Stat.BaseSpeed = 50;

        Stat.Health = Vector2.one * 100;

        Skills.Add(new Skill(10, async (P, S) =>
        {

        }, 3));
    }

    public async Task Passive(float HitCDReductIncrease = 0)
    {
        foreach (var S in Skills)
        {
            S.CoolDown -= (0.5f + HitCDReductIncrease);

            if (S.CoolDown < 0)
            {
                S.CoolDown = 0;
            }
        }
    }
}
